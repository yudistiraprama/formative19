var display = document.getElementById('display');

function add(x) {
    display.value += x;
    if (x == 'C') {
        display.value = '';
    }
}

function answer() {
    results = display.value;
    results = results.replace(/×/g, "*")
    results = eval(results);
    display.value = results;
}

function backspace() {
    var number = display.value;
    var len = number.length - 1;
    var newNumber = number.substring(0, len);
    display.value = newNumber;
}

function power(y) {
    pwr = display.value;
    display.value = '';
    pwr = Math.pow(pwr, y);
    display.value = pwr;
}